﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeneKlass
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene
            {
                IK = "35503070211",
                Nimi = "henn sarv"
            };
            Console.WriteLine($"nimi on: {henn.Nimi}");
            Console.WriteLine($"sünniaeg on: {henn.SünniAeg}");
            Console.WriteLine($"vanus on: {henn.Vanus}");
            Console.WriteLine($"sugu on: {henn.Sugu}");

            Inimene ants = new Inimene()
            {
                IK = "50001010000",
                Nimi = "ants saunamees"
            };

            new Inimene ("60001010000")
            {
                Nimi = "malle mallikas"
            };

            Console.WriteLine($"inimesi on maailmas {Inimene.InimesteArv} tükki");

            foreach(Inimene i in Inimene.Inimesed)
                Console.WriteLine(i);

        }
    }
}

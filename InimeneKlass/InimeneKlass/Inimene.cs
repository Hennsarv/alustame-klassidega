﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeneKlass
{
    /*
    property Nimi (suure algustähega sisu)
    field Isikukood - Pin
    readonly Property Vanus - Age
    readonly Property Sugu - Gender
    readonly Property Sünniaeg -DateOfBIrth

    */
    enum Sugu { Naine, Mees}
    class Inimene
    {
        // staatilised väljad
        public static List<Inimene> Inimesed = new List<Inimene>();
        private static int Loendur = 0;

        // staatiline readonly property
        public static int InimesteArv => Inimesed.Count;

        // siit algavad objekti (instantsi) väljad
        private string _IK = ""; // mõtle, mis oleks teisiti kui IK oleks long
        private string _Nimi;
        private readonly int _Number = ++Loendur;
        //private DateTime _Sünniaeg;


        // konstruktor - see on meetod, mille käivitab (muu hulgas) NEW
        public Inimene() : this("", "tundmatu") { }

        public Inimene(string ik) : this(ik, "tundmatu") { }
 
        public Inimene(string ik, string nimi)
        {
            IK = ik;
            Nimi = nimi;
            Inimesed.Add(this);
        }



        // seni kui ma kirju loen - vaata, mis juhtus IK-ga
        // ja proovi seletada, miks ma seda tegin - mis oleks eesmärk
        public string IK
        {
            get => this._IK;
            set 
            {
                _IK = _IK == "" && value.Length == 11 ? value : _IK;
                if (_IK != "")
                SünniAeg = new DateTime
            (
            ((IK[0] - '1') / 2 + 18) * 100 + int.Parse(IK.Substring(1, 2)),
            int.Parse(IK.Substring(3, 2)),
            int.Parse(IK.Substring(5, 2))
            );

            }
        }

        // kontrollitud väli, mille muutmisega tegeleb meie meetod
        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = ToProper(value); // nimi pannakse alati õigele kujule
        }

        // readonly property
        public Sugu Sugu => (Sugu)(IK[0] % 2);

        public DateTime

            SünniAeg
        {
            get;
            //=> new DateTime
            //(
            //((IK[0] - '1') / 2 + 18) * 100 + int.Parse(IK.Substring(1, 2)),
            //int.Parse(IK.Substring(3, 2)),
            //int.Parse(IK.Substring(5, 2))
            //);
            private set;
        }
        public int Vanus => (DateTime.Today - SünniAeg).Days * 4 / 1461;

        public static string ToProper(string tekst)
        {
            List<string> vastus = new List<string>();
            foreach (var x in tekst.Split(' '))
                if (x != "") vastus.Add(x.Substring(0, 1).ToUpper() + x.Substring(1).ToLower());
            return string.Join(" ", vastus);
        }

        public override string ToString()
        => $"{_Number}. ({IK}) {Sugu} {Nimi}";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaamatJaAutor
{
    class Autor
    {
        string _Nimi = "";
        public List<Raamat> Raamatud = new List<Raamat>();

        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = _Nimi != "" ? _Nimi : ToProper(value);
        }

        public static string ToProper(string tekst)
            => string.Join(" ", tekst.Split(' ')
            .Where(x => x != "")
            .Select(x => x.Substring(0, 1).ToUpper() + x.Substring(1).ToLower())
                );
    }
}

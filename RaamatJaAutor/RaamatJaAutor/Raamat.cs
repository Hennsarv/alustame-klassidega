﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaamatJaAutor
{
    class Raamat
    {
        string _Pealkiri = "";
        double _Hind = 10;
        List<Autor> _Autorid = new List<Autor>();

        public string Pealkiri
        {
            get => _Pealkiri;
            set => _Pealkiri = Autor.ToProper(value);
        }

        public double Hind
        {
            get => _Hind;
            set => _Hind = value >= 10 && value <= 100 ? value : _Hind;
        }

        public void LisaAutor(Autor a)
        {
            _Autorid.Add(a);
            a.Raamatud.Add(this);
        }

        public string Autorid
        {
            get
            {
                List<string> vastus = new List<string>();
                foreach (Autor a in _Autorid) vastus.Add(a.Nimi);
                return string.Join(", ", vastus);
            }
        }
    }
}

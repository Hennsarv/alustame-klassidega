﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MeetodJaFunktsioon.Funktsioonid;
using static System.Math;

using static System.Console;

namespace MeetodJaFunktsioon
{
   
    static class Funktsioonid
    {
        // tegemist on funktsiooniga, mis liidab kaks arvu
        // x ja y ning annab vastuseks nende summa
        // x ja y on selle funktsiooni parameetrid
        //public static int Liida(int x, int y) => x + y;
 
        //public static int Liida(int x , int y, int z = 5)
        //{
        //    return x + y + z;
        //}

        public static int Liida(params int[] arvud)
        {
            return arvud.Sum();
        }

        public static void Trüki(long x, string nimi)
        => WriteLine($"{nimi}={x}");
        

        public static int[] Jada (int suurus, int väärtus)
        {
            int[] vastus = new int[suurus];
            for (int i = 0; i < suurus; i++) vastus[i] = väärtus;
            return vastus;
            
        }

        public static void Arvuta(int x, int y, out int korrutis, out int summa)
        {
            korrutis = x * y;
            summa = x + y;
        }

        public static double Keskmine(int[] arvud, out double summa)
        {
            summa = 0;
            foreach (var x in arvud) summa += x;
            return summa / arvud.Length;
        }

        public static void Suurenda(ref int x, ref int y)
        {
            x++;
            y++;
        }

        
        
    }

    static class E
    {
        public static string Ühenda(this IEnumerable<string> osad, string eraldaja)
            => string.Join(eraldaja, osad);

    }

    static class Ülesanne
    {
        /*
        1. proovi teha funktsioon, mis leiab stringide massiivist keskmise
	(selle, kellest eespool ja tagapool on enamvähem samapalju)
	(selle nimi on nimekirja mediaan)
         */

        public static string KeskmineNimi(this string[] Nimed)
        {
            if (Nimed.Length == 0) return "";
            string[] ajutine = (string[])Nimed.Clone();
            Array.Sort(ajutine);
            return ajutine[ajutine.Length / 2];
        }

        /*3. proovi teha funktsioon, millele antakse ette massiiv
	või list inimestega (Inimene {Nimi, Sugu})
	ja mis annab tagasi kaks inimest - ühe juhusliku mehe
	ja ühe juhusliku naise nimekirjast
	(selle nimi on paaripanemise funktsioon)*/

    public static Inimene[] JuhuslikPaar(Inimene[] kellest)
        {
            List<Inimene> mehed = new List<Inimene>();
            List<Inimene> naised = new List<Inimene>();

            foreach (Inimene i in kellest)
                if (i.Sugu == Sugu.Mees) mehed.Add(i);
                else naised.Add(i);

            if (mehed.Count == 0 || naised.Count == 0) return null;

            Random r = new Random();
            int mitmesMees = r.Next(mehed.Count);
            int mitmesNaine = r.Next(naised.Count);

            return new Inimene[]
            {
                mehed[mitmesMees],
                naised[mitmesNaine]
            };

        }



    }
    enum Sugu { Naine, Mees}
    class Inimene { public string Nimi; public Sugu Sugu; }

    class Program
    {
        static void Main(string[] args)
        {
            #region peitu
            foreach (string x in args) Console.WriteLine(x);

            // siin me kasutame seda funktsiooni avaldises
            // 4 ja 7 on selle funktsiooni argumendid
            //int summa = Liida(4, 7);
            Trüki(Liida(new int[] { 1, 2, 3, 10, 17 }), "Summa");

            int s;
            int k;
            Arvuta(4, 7, out k, out s);
            Console.WriteLine($"summa on {s} ja korrutis {k}");

            int[] arvud = { 1, 2, 3, 4, 5, 6, 5, 34, 2, 9 };
            double kesk = Keskmine(arvud, out double sum);
            Console.WriteLine($"summa on {sum} ja keskmine {kesk}");

            int a = 7;
            int b = 8;

            Suurenda(ref a, ref a);

            Console.WriteLine($"a on {a}");

            

            A ahaa = new A();
            ahaa.Arv = 7;
            ahaa.Tryki();
            A.Tryki(ahaa);

            string nimi = "henn sarv on ilus poiss";
            string tulemus =
                new string(
                nimi
                //.ToUpper()
                //.Substring(5,4)
                .Reverse()
                .ToArray()
                )
                ;
            Console.WriteLine(tulemus);

            string[] tykid = { "Henn", "on", "ilus", "poiss" };

            //            string koos = E.Ühenda(tykid, ", ");

            string koos = tykid.Ühenda(", ");
            Console.WriteLine(koos);

            #endregion

            // leiame massiivist keskmise nime
            string[] nimed = { "Henn", "Ants", "Peeter", "Juhan", "Tiit" };
            Console.WriteLine(nimed.KeskmineNimi());

            // leiame juhusliku paari
            Inimene[] inimesed =
            {
                new Inimene {Nimi = "Henn", Sugu = Sugu.Mees},
                new Inimene {Nimi = "Pille", Sugu = Sugu.Naine},
                new Inimene {Nimi = "Ants", Sugu = Sugu.Mees},
                new Inimene {Nimi = "Peeter", Sugu = Sugu.Mees},
                new Inimene {Nimi = "Malle", Sugu = Sugu.Naine},
                new Inimene {Nimi = "Sille", Sugu = Sugu.Naine},
                new Inimene {Nimi = "Kalle", Sugu = Sugu.Mees},
                new Inimene {Nimi = "Ülle", Sugu = Sugu.Naine},
                new Inimene {Nimi = "Joosep", Sugu = Sugu.Mees}
            };

            

            Inimene[] paar = Ülesanne.JuhuslikPaar(inimesed);
            Console.WriteLine($"paari lähevad {paar[0].Nimi} ja {paar[1].Nimi}");


        }
    }

    class A
    {
        public int Arv;

        public void Tryki() => Console.WriteLine($"arv on {this.Arv}");

        public static void Tryki(A a) 
            => Console.WriteLine($"arv on {a.Arv}");

    }
}

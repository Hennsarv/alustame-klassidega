﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NäideAndmetega
{
    class Program
    {
        static void Main(string[] args)
        {
            northwindEntities ne = new northwindEntities();
            foreach(Product p in ne.Products)
                Console.WriteLine(p.ProductName);

            foreach(Employee e in ne.Employees)
                Console.WriteLine($"{e.FullName} ülemus on {e.Manager?.FullName}");
        }
    }
}

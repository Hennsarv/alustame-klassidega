﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnKlass
{
    class Inimene
    {
        private string _Nimi;
        public DateTime Sünniaeg { get; set; }

        public int Vanus
        {
            get
            {
                int vanus = DateTime.Now.Year - Sünniaeg.Year;
                if (Sünniaeg.AddYears(vanus) > DateTime.Today) vanus--;
                return vanus;
            }
        }

        //public DateTime Sünniaeg
        //{
        //    get => _Sünniaeg;
        //    set => _Sünniaeg = value;
        //}

        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = ToProper(value.Split(' '));
        }

        public string GetNimi() => _Nimi;

        public void SetNimi(string nimi) => _Nimi = ToProper(nimi.Split(' '));

        public string ProperName() =>ToProper(_Nimi.Split(' '));
        

        public static string ToProper(string tekst)
        =>
                tekst == "" ? "" :
                tekst.Substring(0, 1).ToUpper() + tekst.Substring(1).ToLower();
        
        public void TrükiNimi() => Console.WriteLine(ToProper(_Nimi.Split(' ')));

        public static string ToProper(string[] tekst)
        {
            List<string> vastus = new List<string>();
            foreach (string sõna in tekst) if (sõna != "") vastus.Add(ToProper(sõna));
            return string.Join(" ", vastus);

        }


        #region peitu

        public override string ToString()
      => $"inimene nimega {_Nimi} on {Vanus()} aastane";

        #endregion


    }
}

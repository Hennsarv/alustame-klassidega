﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SpordipäevaÜlesanne
{
    class Protok
    {
        public string Nimi;
        public int Distants;
        public int Aeg; // sekundites
        public double Kiirus;
    }

    class Program
    {
        static void Main(string[] args)
        {
            string fileNimi = "spordipäeva protokoll";
            string fileName = $@"..\..\{fileNimi}.txt";
            // tahtsin proovida, kas $ ja @ saab koos kasutada string ees
            // selgub, et saab, aga siis tuleb neid kasutada ÕIGES järjekorras
            // enne $ ja siis @

            string[] lines = File.ReadAllLines(fileName);

            #region eelmine
            //string[] nimed = new string[lines.Length-1];
            //int[] distantsid = new int[lines.Length-1];
            //int[] ajad = new int[lines.Length-1];
            //double[] kiirused = new double[lines.Length-1];

            //for (int i = 0; i < lines.Length-1; i++)
            //{
            //    string line = lines[i+1];
            //    string[] osad = line.Split(',');
            //    nimed[i] = osad[0].Trim();
            //    distantsid[i] = int.Parse(osad[1].Trim());
            //    ajad[i] = int.Parse(osad[2].Trim());
            //    kiirused[i] = (double)distantsid[i] / ajad[i];
            //}

            //// siit tuleks nüüd edasi minna
            //// kiireim aeg
            //Console.WriteLine("\nKõige kiirem jooksja:");
            //double kiireim = kiirused.Max();
            //for (int i = 0; i < kiirused.Length; i++)
            //    if (kiirused[i] == kiireim) Console.WriteLine(nimed[i]);
            //// kuidas leida kiireim 100m aeg (sprinter) ja selle jooksja

            //Console.WriteLine("\nKõige kiirem sprinter:");
            //double maxSprinter = 0;
            //for (int i = 0; i < kiirused.Length; i++)
            //{
            //    if (distantsid[i] == 100 && kiirused[i] > maxSprinter) maxSprinter = kiirused[i]; 
            //}

            //for (int i = 0; i < kiirused.Length; i++)
            //{
            //    if (kiirused[i] == maxSprinter && distantsid[i] == 100)
            //        Console.WriteLine(nimed[i]);
            //} 
            #endregion

            List<Protok> protokoll = new List<Protok>();
            for (int i = 1; i < lines.Length; i++)
            {
                string[] osad = lines[i].Split(',');
                int d = int.Parse(osad[1].Trim());
                int a = int.Parse(osad[2].Trim());
                protokoll.Add(
                new Protok
                {
                    Nimi = osad[0],
                    Distants = d,
                    Aeg = a,
                    Kiirus = (double)d / a
                }

                );
            }
            //foreach (Protok p in protokoll) p.Kiirus = (double)p.Distants / p.Aeg;

            // ühte asja me pole veel õppinud :)
            //double maxKiirus1 = protokoll.Max(x => x.Kiirus);
            //double maxSprinter1 = protokoll.Where(x => x.Distants == 100).Max(x => x.Kiirus);
            // seda me siis ei kasuta
            double maxKiirus = 0;
            double maxSprinter = 0;
            foreach(Protok p in protokoll)
            {
                if (p.Kiirus > maxKiirus) maxKiirus = p.Kiirus;
                if (p.Kiirus > maxSprinter && p.Distants == 100) maxSprinter = p.Kiirus;

            }
            foreach(Protok p in protokoll)
            {
                if (p.Kiirus == maxKiirus)
                    Console.WriteLine($"{p.Nimi} on kiireim");
                if (p.Kiirus == maxSprinter)
                    Console.WriteLine($"{p.Nimi} on kiireim sprinter");
            }

        }


    }
}

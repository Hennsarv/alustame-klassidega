﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaamatJaAutor2
{
    class Program
    {
        static void Main(string[] args)
        {
            Autor mark = "mark twain"; // see ei ole päris ilus, mis ma tegin
            // aga seda saab teha tänu vigurile, mis on Autori klassi lõpus
            // vaata, aga ära seda usu - nii tavaliselt ei tehta

            Autor agata = new Autor("agatha christie" );
            Autor ilf = new Autor("ilf");
            Autor petrof = new Autor( "petroff" );
            Console.WriteLine(mark.Nimi);
            Console.WriteLine(agata.Nimi);
            Console.WriteLine(ilf.Nimi);
            Console.WriteLine(petrof.Nimi);

            Raamat p1 = new Raamat { Pealkiri = "poirot peseb pead" };
            p1.LisaAutor(agata);
            Raamat p2 = new Raamat { Pealkiri = "idaekspress" };
            p2.LisaAutor(agata);
            Raamat p3 = new Raamat { Pealkiri = "10 neegrit" };
            p3.LisaAutor(agata);

            Raamat t12 = new Raamat { Pealkiri = "12 tooli" };
            t12.LisaAutor(ilf);
            t12.LisaAutor(petrof);

            Console.WriteLine("\nilfi raamatud\n");
            foreach (var r in ilf.Raamatud)
            {
                Console.WriteLine(r.Pealkiri);
            }

            Console.WriteLine("\nagatha raamatud\n");
            foreach (var r in agata.Raamatud)
            {
                Console.WriteLine(r.Pealkiri);
            }

            Console.WriteLine($"\nraamatu {t12.Pealkiri} autorid on:");
            Console.WriteLine(t12.AutoriteNimekiri2);

            Console.WriteLine($"\nraamatu {p3.Pealkiri} autor on");
            Console.WriteLine(p3.AutoriteNimekiri2);

            Console.WriteLine("\nkasutame tostring meetodit");
            Console.WriteLine(t12);
            Console.WriteLine(p3);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaamatJaAutor2
{
    class Autor
    {
        string _Nimi = "";
        public List<Raamat> Raamatud = new List<Raamat>();

        public string Nimi
        {
            get => _Nimi;
            private set => _Nimi =
                // _Nimi != "" ? _Nimi : // kuna me ainult klassi sees muudame, siis selle kontrolli võib ära jätta
                Raamat.ToProper(value);
        }

        public Autor(string nimi)
        {
            Nimi = nimi;
        }

        // seda vigurit me õppinud ei ole ja ma ei plaani ka seletada
        public static implicit operator Autor(string nimi)
            => new Autor(nimi);

    }
}

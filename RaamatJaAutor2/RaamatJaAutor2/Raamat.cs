﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaamatJaAutor2
{
    class Raamat
    {
        string _Pealkiri = "";
        double _Hind = 10;
        List<Autor> _Autorid = new List<Autor>();
        
        public string Pealkiri
        {
            get => _Pealkiri;
            set => _Pealkiri = ToProper(value);
        }

        public double Hind
        {
            get => _Hind;
            set => _Hind = value >= 10 && value <= 100 ? value : _Hind;
        }

        public void LisaAutor(Autor autor)
        {
            _Autorid.Add(autor);
            autor.Raamatud.Add(this);
        }

        public string AutoriteNimekiri
        {
            get
            {
                List<string> vastus = new List<string>();
                foreach (var a in _Autorid)
                {
                    vastus.Add(a.Nimi);
                }
                return string.Join(", ", vastus);
            }
        }

        // see on autorite nimekirja kokkupanemine pisut teisel
        // (lihtsamal) kujul, aga seda me veel ei oska
        // põhimõtteliselt teeb sama, mis eelmine
        public string AutoriteNimekiri2
            => string.Join(", ", _Autorid.Select(x => x.Nimi));

        public static string ToProper(string tekst)
        {
            List<string> vastus = new List<string>();
            foreach (string x in tekst.Split(' '))
                if (x != "")
                    vastus.Add(x.Substring(0, 1).ToUpper() + x.Substring(1).ToLower());
            return string.Join(" ", vastus);
        }

        public override string ToString()
            => $"{Pealkiri} ({AutoriteNimekiri2}) maksab: {Hind}";

        public static string ToProper2(string tekst)
            => string.Join(" ", tekst
                .Split(' ')
                .Where(x => x != "")
                .Select(x => x.Substring(0, 1).ToUpper() + x.Substring(1).ToLower())
                );
    }
}

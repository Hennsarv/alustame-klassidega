﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Õpilane : Inimene
    {
        public string KlassKusÕpib;

        public override string ToString()
        => $"{KlassKusÕpib} klassi õpilane " + base.ToString();
    }

    class Õpetaja : Inimene
    {
        public string AineMidaÕpetab;

        public override string ToString()
        => $"{AineMidaÕpetab} õpetaja " + base.ToString();
    }
}

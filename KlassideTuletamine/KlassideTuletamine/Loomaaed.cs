﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    abstract class ElusOlend
    {
        public abstract void TeeHäält();
    }

    class Loom : ElusOlend
    {
        public string Liik;
        public Loom(string liik = "tundmatu") => Liik = liik;
        public override string ToString() => $"{Liik}";
        public override void TeeHäält()
        => Console.WriteLine($"{this} teeb himsat häält"  );

    }

    class Koduloom : Loom
    {
        public string Nimi;
        public Koduloom(string liik = "pudulojus", string nimi = "nimeta")
            : base(liik)
            => Nimi = nimi;
        public override string ToString()
        => $"{Liik} {Nimi}";
        public override void TeeHäält()
        {
            switch (this.Liik)
            {
                case "lehm": Console.WriteLine( "Amuuu"); break;
                case "hobune": Console.WriteLine( "Hirnu"); break;
                default: Console.WriteLine("me ei tea mis häält"); ; break;
            };
        }
    }

    sealed class Kass : Koduloom, ISöödav
    {
        public string Tõug;
        public bool Tuju = true;
        public Kass(string tõug = "rääbu", string nimi = "Kõuts")
            : base("kass", nimi)
            => Tõug = tõug;
        public void SikutSabast() => Tuju = false;
        public void Silita() => Tuju = true;

        public override string ToString()
        => $"{Tõug} kass {Nimi}";

        public override void TeeHäält()
        {
            if (Tuju)
                Console.WriteLine($"{this} lööb nurru");
            else
                Console.WriteLine($"{this} kräunub koledasti");
        }

        public void Süüakse()
        {
            Console.WriteLine($"{this} pistetakse nahka") ;
        }
    }

    class Koer : Koduloom
    {
        public string Tõug;

        public Koer(string tõug, string nimi)
            : base("koer", nimi)
            => Tõug = tõug;


        public override void TeeHäält()
            => Console.WriteLine($"{this} haugub");

    }

    interface ISöödav
    {
        void Süüakse();
    }

    class Sepik : ISöödav
    {
        public void Süüakse() => Console.WriteLine("keegi nosib sepikut");
        

    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    static class Program
    {
        static void Main(string[] args)
        {
            #region Peitu
            new Inimene { Nimi = "Henn" };
            new Inimene { Nimi = "Ants" };



            Õpilane õ = new Õpilane
            {
                Nimi = "Malle",
                KlassKusÕpib = "1b",
                Vanus = 10
            };

            Õpetaja õps = new Õpetaja
            {
                Nimi = "Mati",
                AineMidaÕpetab = "matemaatika",
                Vanus = 40
            };

            foreach (Inimene i in Inimene.Inimesed)
            {

                //Console.WriteLine(i);
            }
            #endregion

            //Loom l1 = new Loom();
            //Loom l2 = new Loom("krokodill");
            //Loom l3 = new Loom { Liik = "laiskelajas" };
            //Console.WriteLine(l3);
            //l2.TeeHäält();

            Koduloom k1 = new Koduloom("lehm", "Maasik");
            Console.WriteLine(k1);
            k1.TeeHäält();

            Kass m1 = new Kass("angoora", "Miisu");
            Console.WriteLine(m1);
            m1.SikutSabast();
            m1.TeeHäält();

            Koer p1 = new Koer("Granze", "Pontu");
            p1.TeeHäält();

            Sepik s1 = new Sepik();

            Lõuna(s1);
            Lõuna(m1);

            Loom lx = m1;
            Lõuna(p1);

        }

        public static void Lõuna(object x)
        {
            if (x is ISöödav toit) toit.Süüakse();
            else Console.WriteLine("seekord jääme nälga");
        }

    }
}

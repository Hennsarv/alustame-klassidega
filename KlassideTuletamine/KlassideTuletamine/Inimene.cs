﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Inimene
    {
        // static väli - kõigile ühine
        // seda on klassi kohta täpselt 1
        public static List<Inimene> Inimesed = new List<Inimene>();

        // objekti (Inimese) väli
        // see on igal inimesel oma
        public string Nimi;
        public int Vanus;

        // static konstruktor on ilma juurdepääsuta (private/public)
        // ilma parameetriteta
        // teda saab olla AINULT ÜKS
        static Inimene()
        {
            // tema ülesanne on valmistada ette ja väärtustada 
            // staatilised väljad

        }

        // inimese konstruktor
        public Inimene() => Inimesed.Add(this);

        public override string ToString()
        => $"Inimene {Nimi} (vanus: {Vanus})";

    }
}

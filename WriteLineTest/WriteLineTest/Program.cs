﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WriteLineTest
{
    class Program
    {
        static void Main(string[] args)
        {
            double pi = Math.PI;

            Console.WriteLine($"Pi on {pi : 0.00}");
            Console.WriteLine("Pi on {0:0.00}", pi);
            string piStr = pi.ToString("0.00");
            Console.WriteLine(piStr);

            DateTime täna = DateTime.Today;

            Console.WriteLine($"{täna: dddd}");

            // on KOLM kohta, kus sa saad kasutada formaatimisstringi
            // arvude ToString( ... ) <- seal sulgude sees
            // WriteLine("    {0: ....}", ..) <- seal placeholderi sees : järel
            // $"   {avaldis : ....}   " <- interpoleeritud stringis : järel {} vahel
            // arvu formaadi mustrist vaata:
            // https://docs.microsoft.com/en-us/dotnet/standard/base-types/formatting-types

        }
    }
}

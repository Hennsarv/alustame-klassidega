﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeelkordInimene
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Inimene henn = ("Henn", "Sarv", 63);
            Console.WriteLine(henn);

            // niimoodi lihtslat EI TEHTA!
            List<Inimene> klass = new List<Inimene>
            {
                "Toomas Linnupog",
                "Kiilike",
                "Joonas Joonike"
            };

            foreach (var x in klass) Console.WriteLine(x);

            //Inimene i2 = new Inimene();
            //Console.WriteLine(i2);



            //Console.WriteLine(i1.Täisnimi);

            Inimene malle = "Malle Malllikas";
            Inimene kalle = "Kalle Kaalikas";
            ;
            Console.WriteLine(kalle + malle);

        }
    }
}

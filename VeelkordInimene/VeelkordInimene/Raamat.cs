﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeelkordInimene
{
    class Raamat
    {
        public string Pealkiri { get; set; } = "pealkirjata raamat";
        public int Ilmumisaasta { get; set; } = 2000;
        public List<Inimene> Autorid{ get; set; } = new List<Inimene>();

        // seda me järgmisel nädalal kunagi vaatame
        public string AutoriNimed
            => string.Join(", ", Autorid.Select(x => x.Täisnimi));
    }
}

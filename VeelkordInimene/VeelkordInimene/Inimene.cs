﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeelkordInimene
{
    class Inimene
    {
        private static List<Inimene> _Inimesed = new List<Inimene>();

        public string Eesnimi { get; private set; }
        public string Perenimi { get; private set; }
        int _Vanus;
        public int Vanus { get => _Vanus; set => _Vanus = value; }

        public Inimene Ema;
        public Inimene Isa;

        // KONSTRUKTOR: 
        // meetod, millel on klassi nimi
        // millel ei ole vastuse tüüpi (ega voidi)
        // mida ei saa OTSE käivitada
        // mida käivitab new operatsioon
        private Inimene(string eesnimi, string perenimi) : this()
        {
            Eesnimi = eesnimi;
            Perenimi = perenimi;
            //(Eesnimi, Perenimi) = (eesnimi, perenimi);
        }

        // : this võimaldab siduda konstruktorid ühte jadasse
        // nii et nad täidetakse üksteise järel
        // standardne majapidamine koguda ühte ja siduda see teiste külge

        public Inimene() => _Inimesed.Add(this);
        //{
        //    _Inimesed.Add(this);
        //}

        public string Täisnimi => Eesnimi + " " + Perenimi;
        //{
        //    get => Eesnimi + " " + Perenimi;
            
        //}

        //public string TäisNimi() 
        //    => Eesnimi + " " + Perenimi;
        ////{
        ////    return Eesnimi + " " + Perenimi;
        ////}
        



        public override string ToString() 
            => $"{Eesnimi} {Perenimi} ({Vanus})";
        //{
        //    // return Eesnimi + " " + Perenimi + " (" + Vanus + ")";
        //    return $"{Eesnimi} {Perenimi} ({Vanus})";
        //}

            // PALUN JUMALA PÄRAST ÄRA ARVA, ET NII PEAB TEGEMA
        public static implicit operator Inimene(string nimed)
        => new Inimene((nimed+" ").Split(' ')[0], (nimed + " ").Split(' ')[1]);
        public static implicit operator 
            Inimene((string eenimi, string perenimi, int vanus) x) 
            => new Inimene(x.eenimi, x.perenimi) { Vanus = x.vanus};

        // klassitunnis uute inimeste tegemist meil ei õpetata selliselt
        public static Inimene operator+ (Inimene i, Inimene e)
        => new Inimene(i.Eesnimi + " ja " + e.Eesnimi + " laps", i.Perenimi) { Isa = i, Ema = e };

    }
}
